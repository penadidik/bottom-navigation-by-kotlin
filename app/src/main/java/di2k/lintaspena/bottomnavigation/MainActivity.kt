package di2k.lintaspena.bottomnavigation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import di2k.lintaspena.bottomnavigation.fragment.AccountFragment
import di2k.lintaspena.bottomnavigation.fragment.FavoriteFragment
import di2k.lintaspena.bottomnavigation.fragment.HomeFragment
import di2k.lintaspena.bottomnavigation.fragment.SearchFragment//Created by 디딬 Didik M. Hadiningrat on 21 July 2019

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // kita set default nya Home Fragment
        loadFragment(HomeFragment())
        // inisialisasi BottomNavigaionView
        val bottomNavigationView : BottomNavigationView = findViewById(R.id.bn_main)
        // beri listener pada saat item/menu bottomnavigation terpilih
        bottomNavigationView.setOnNavigationItemSelectedListener(this@MainActivity)
    }

    private fun loadFragment(fragment: Fragment?): Boolean{
        if(fragment != null){
           supportFragmentManager.beginTransaction()
               .replace(R.id.fl_container, fragment)
               .commit()
            return true
        }
        return false
    }

    override fun onNavigationItemSelected(@NonNull menuItem: MenuItem): Boolean {
        var fragment: Fragment? =null
        when (menuItem.itemId) {
            R.id.home_menu -> fragment = HomeFragment()
            R.id.search_menu -> fragment = SearchFragment()
            R.id.favorite_menu -> fragment = FavoriteFragment()
            R.id.account_menu -> fragment = AccountFragment()
        }
        return loadFragment(fragment)


    }
}