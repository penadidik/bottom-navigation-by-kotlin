package di2k.lintaspena.bottomnavigation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import di2k.lintaspena.bottomnavigation.R

class HomeFragment: Fragment() {//Created by 디딬 Didik M. Hadiningrat on 21 July 2019

    @Nullable
    @Override
    override fun onCreateView(@NonNull inflater: LayoutInflater, @NonNull container: ViewGroup?, @NonNull savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)
        return view
    }
}