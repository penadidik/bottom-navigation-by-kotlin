package di2k.lintaspena.bottomnavigation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import di2k.lintaspena.bottomnavigation.R

class SearchFragment: Fragment() {

    @Nullable
    @Override//Created by 디딬 Didik M. Hadiningrat on 21 July 2019
    override fun onCreateView(@NonNull inflater: LayoutInflater, @NonNull container: ViewGroup?, @NonNull savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_search, container, false)
        return view
    }
}